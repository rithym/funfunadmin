import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorFoodsComponent } from './vendor-foods.component';

describe('VendorFoodsComponent', () => {
  let component: VendorFoodsComponent;
  let fixture: ComponentFixture<VendorFoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorFoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorFoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
