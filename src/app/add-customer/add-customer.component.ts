import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl,
} from "@angular/forms";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Location } from "@angular/common";
// import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
// const stripe = require('stripe')('sk_test_h2KWJ6tZJ51osA8TJG83eE8000g92mj71d');
import { CreditCardValidator } from "angular-cc-library";

@Component({
  selector: "app-add-customer",
  templateUrl: "./add-customer.component.html",
  styleUrls: ["./add-customer.component.css"],
})
export class AddCustomerComponent implements OnInit {
  form: FormGroup;
  submitted: boolean = false;

  authForm: FormGroup;
  Image: any = null;
  udata: any = "";
  selectedFile: any;
  allInterests: any = "";
  creditCardForm: FormGroup;
  constructor(
    public apiService: ApiService,
    public fb: FormBuilder,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService,
    private _location: Location
  ) {
    this.createForm();
    //  this.getInterestsAreas();
  }

  ngOnInit() {
    // 	this.creditCardForm = this.fb.group({
    // 		cardName : ['',[Validators.required]],
    // 		number: ['', [Validators.required,<any>CreditCardValidator.validateCCNumber]],
    // 		expMonth: ['', [Validators.required ,<any>CreditCardValidator.validateExpDate]]
    // 	},{});
    // }
    // getInterestsAreas(){
    // 	this.apiService.getAllInterests().subscribe((data)=>{
    // 		console.log(data);
    // 		this.allInterests = data;
    // 	})
  }

  /***********PASSWORD MATCHING********** */

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[field_name] == input;
      if (!isValid) return { equalTo: { isValid } };
      else return null;
    };
  }

  createForm() {
    this.authForm = this.fb.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      username: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      gender: ["", [Validators.required]],
      dob: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(8)]],
      cpassword: ["", [Validators.required, this.equalto("password")]],
    });
  }

  //  createCreditCardForm(){

  //   }

  // updateInterestAreas(userid) {
  //   var data = {
  //     userid: userid,
  //     interests: this.interestArr,
  //   };
  //   this.apiService.updateUserInterests(data).subscribe((res) => {
  //     console.log(res);
  //   });
  // }

  addCustomer() {
    console.log("add cust");
    this.saveCustomerDetails(this.authForm);
    document.getElementById("contact-tab1").click();
  }

  // onImageChanged(event) {
  //   console.log(event);
  //   let reader = new FileReader();
  //   this.Image = event.target.files[0];
  //   this.selectedFile = event.target.files[0];
  //   let extn = event.target.value.split(".")[1];
  //   if (extn == "jpg" || extn == "png" || extn == "jpeg") {
  //     reader.onload = () => {
  //       this.Image = reader.result;
  //     };
  //     reader.readAsDataURL(event.target.files[0]);
  //   } else if (extn == "pdf") {
  //     this.Image = event.target.files[0].name;
  //   }

  //   //this.authForm.patchValue({profile_image : this.Image});
  // }

  // uploadImage() {
  //   this.spinnerService.show();
  //   let dict = {
  //     email: this.authForm.value.email,
  //     item_type: "customer",
  //     new: true,
  //   };
  //   this.apiService.is_email_exist(dict).subscribe((response) => {
  //     if (response.status == 1) {
  //       var payload = new FormData();
  //       payload.append("image", this.selectedFile, this.selectedFile.name);
  //       this.apiService.UploadImage(payload).subscribe((response) => {
  //         console.log(response);
  //         this.authForm.value.profile_image = response.image;
  //         console.log(this.authForm.value);
  //         this.Image = null;
  //         this.addCustomer();
  //       });
  //     } else {
  //       this.spinnerService.hide();
  //       this.toastr.error(response.msg);
  //     }
  //   });
  // }

  // /*************SELECT INTERESTS*************** */
  // interestArr: any = [];

  // selectInterests(interest, id) {
  //   console.log(interest);
  //   console.log(id);
  //   console.log(this.interestArr);

  //   if (this.interestArr.length > 0) {
  //     for (var i = 0; i < this.interestArr.length; i++) {
  //       if (interest == this.interestArr[i].interest) {
  //         this.interestArr.splice(i, 1);
  //         console.log(this.interestArr);
  //         //   this.updateInterestAreas();
  //         break;
  //       } else if (
  //         interest != this.interestArr[i].interest &&
  //         i == this.interestArr.length - 1
  //       ) {
  //         this.interestArr.push({
  //           interest: interest,
  //           _id: id,
  //           checked: true,
  //         });
  //         console.log(this.interestArr);
  //         //   this.updateInterestAreas();
  //         break;
  //       }
  //     }
  //   } else {
  //     this.interestArr.push({
  //       interest: interest,
  //       _id: id,
  //       checked: true,
  //     });
  //     console.log(this.interestArr);
  //     // this.updateInterestAreas();
  //   }
  // }

  // randomString() {
  //   let charSet = "";
  //   let len = 10;
  //   charSet =
  //     charSet ||
  //     "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  //   var randomString = "";
  //   for (var i = 0; i < len; i++) {
  //     var randomPoz = Math.floor(Math.random() * charSet.length);
  //     randomString += charSet.substring(randomPoz, randomPoz + 1);
  //     console.log(randomString);
  //   }
  //   return randomString;
  // }

  // validateCard: any = false;
  // validateExpMonthYear: any = false;
  // validateCvv: any = false;

  // getCreditCardType(accountNumber) {
  //   //start without knowing the credit card type
  //   var result = "unknown";

  //   //first check for MasterCard
  //   if (/^5[1-5]/.test(accountNumber)) {
  //     result = "mastercard";
  //   }

  //   //then check for Visa
  //   else if (/^4/.test(accountNumber)) {
  //     result = "visa";
  //   }

  //   //then check for AmEx
  //   else if (/^3[47]/.test(accountNumber)) {
  //     result = "amex";
  //   } else if (/^6(?:011|5[0-9]{2})[0-9]{12}/.test(accountNumber)) {
  //     result = "Discover";
  //   } else if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}/.test(accountNumber)) {
  //     result = "Diners Club";
  //   } else if (/^(?:2131|1800|35\d{3})\d{11}/.test(accountNumber)) {
  //     result = "JCB";
  //   }
  //   return result;
  // }

  // saveCard(uid, value) {
  //   console.log(value.expMonth.split("/")[0]);
  //   var data = {
  //     userid: uid,
  //     card: {
  //       id: this.randomString(),
  //       name: value.cardName,
  //       number: value.number,
  //       expMonth: value.expMonth.split("/")[0],
  //       expYear: value.expMonth.split("/")[1],
  //       cvc: null,
  //       cardtype: this.getCreditCardType(value.number),
  //     },
  //   };
  //   console.log("SAVE PAYMENT DATA");
  //   console.log(data);

  //   // return false;
  //   this.apiService.updatePaymentDetails(data).subscribe((res) => {
  //     console.log(res);
  //   });
  // }

  saveCustomerDetails(value: any): void {
    var dict = {
      firstName: this.authForm.value.firstName,
      lastName: this.authForm.value.lastName,
      dob: this.authForm.value.dob,
      gender: this.authForm.value.gender,
      email: this.authForm.value.email,
      password: this.authForm.value.password,
      username: this.authForm.value.username,
    };
    console.log("DICT", dict);

    this.spinnerService.show();
    this.apiService.add_user(dict).subscribe((response) => {
      this.spinnerService.hide();
      if (response.status === true) {
        console.log(response);
        this.toastr.success("User has been added successfully!");
        this.udata = response.data;
        this.add_points();
        this._location.back();
      } else {
        this.toastr.error("Error in adding user");
      }
    });
  }

  add_points() {
    console.log("NEW CREATED DATA", this.udata);

    const data = {
      userid: this.udata._id,
      username: this.udata.username,
      total_points: 250,
      given_points: 0,
      recived_points: 0,
      level: 1,
    };
    console.log("POINTS DATA", data);
    this.apiService.add_points(data).subscribe((res) => {
      // this.helperService.presentLoading();
      console.log("Response From Points Table", res);
      this.diff_levels(this.udata._id);
      // this.helperService.stopLoading();
    });
    // this.diff_levels();
  }

  diff_levels(userida) {
    console.log(userida);
    const data = {
      userid: userida,
    };
    this.apiService.diff_levels(data).subscribe((res) => {
      console.log("NOTIFICATION RESPONSE", res);
    });
  }
}
