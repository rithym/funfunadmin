import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.css']
})
export class AddVendorComponent implements OnInit {
	authForm: FormGroup; 
	Image: any = null;
	selectedFile: any;
  	constructor(public apiService: ApiService, public fb: FormBuilder, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, private _location: Location) { 
  		this.createForm();
  	}

  	ngOnInit() {
  	}

  	createForm(){
	   	this.authForm = this.fb.group({
		    restaurant_name: ['', Validators.compose([Validators.required])],
		    opening_hours: ['', Validators.compose([Validators.required])],
		    closing_hours: ['', Validators.compose([Validators.required])],
		    password: ['', Validators.compose([Validators.required])],
		    email: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])],
	       	contact: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{4,15}')])],
	      	address_line_1: ['', Validators.compose([Validators.required])],
	      	address_line_2: [''],
	      	zip_code: ['', Validators.compose([Validators.required])],
	  	 	city: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
	 	 	state: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
	 	 	country: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
	      	profile_image: [null],
	      	disable_ordering: [false],
	    });	   
 	};


 	addVendor(){
 		if(this.Image == null){
	 		var dict = {
	 			restaurant_name: this.authForm.value.restaurant_name,
	 			opening_hours: this.authForm.value.opening_hours, 
	 			closing_hours: this.authForm.value.closing_hours, 
	 			email: this.authForm.value.email,
	 			contact: this.authForm.value.contact,
	 			address_line_1: this.authForm.value.address_line_1,
	 			address_line_2: this.authForm.value.address_line_2 == '' ? null : this.authForm.value.address_line_2,
	 			city: this.authForm.value.city,
	 			country: this.authForm.value.country,
	 			zip_code: this.authForm.value.zip_code,
	 			state: this.authForm.value.state,
	 			disable_ordering: this.authForm.value.disable_ordering,
	 			profile_image: this.authForm.value.profile_image,
	 			payment_mathods: [],
	 			password: this.authForm.value.password
	 		};

	 		this.spinnerService.show();
			this.apiService.add_vendor(dict).subscribe((response) => {
				this.spinnerService.hide();
				if(response.status == 1){
					this._location.back();
					//this.navCtrl.dismiss();
					this.toastr.success('Vendor has been added successfully!');
				}else{
					this.toastr.error('Email already exist in our system.Please try another one!');
				}
			});
 		}else{
 			this.uploadImage();
 		}
 		
 	};

 	Tab(activeTab){
 		if(activeTab == 'basic'){
 			let element = document.getElementById("history1");
 			if(element != null){
	  			element.classList.remove("active")
	  			element.classList.remove("show");
	  		}

  			element = document.getElementById("payment1");
  			if(element != null){
	  			element.classList.remove("active")
	  			element.classList.remove("show");
	  		}
 		}else if(activeTab == 'address'){
 			let element = document.getElementById("whoweare");
 			if(element != null){
 				element.classList.remove("active")
  				element.classList.remove("show");
 			}
  			element = document.getElementById("payment1");
 			if(element != null){
	  			element.classList.remove("active")
	  			element.classList.remove("show");
	  		}
 		}else{

 			let element = document.getElementById("history1");
 			if(element != null){
	  			element.classList.remove("active")
	  			element.classList.remove("show");
	  		}
  			element = document.getElementById("whoweare");
  			if(element != null){
	  			element.classList.remove("active")
	  			element.classList.remove("show");
	  		}
 		}
 	}

 	onImageChanged(event) {
  		console.log(event)
	    let reader = new FileReader();
	    this.Image = event.target.files[0];
	    this.selectedFile = event.target.files[0];
	    let extn = event.target.value.split('.')[1];
	    if(extn =='jpg' || extn =='png' || extn =='jpeg'){
	      reader.onload = () => {
	        this.Image = reader.result;
	      };
	      reader.readAsDataURL(event.target.files[0]);               
	    }else if(extn == 'pdf'){
	       this.Image = event.target.files[0].name;
	    }

	    //this.authForm.patchValue({profile_image : this.Image});
  	};

  	uploadImage(){
	    this.spinnerService.show();
	    let dict = {
	    	'email': this.authForm.value.email,
	    	'item_type': 'vendor',
	    	'new': true
	    };
	    this.apiService.is_email_exist(dict).subscribe((response) => {
	      if(response.status == 1){
	        var payload = new FormData();
	        payload.append('image', this.selectedFile, this.selectedFile.name);
	        this.apiService.UploadImage(payload).subscribe((response) => {
	          console.log(response);
	          this.authForm.value.profile_image = response.image;
	          console.log(this.authForm.value);
	          this.Image = null;
	          this.addVendor();
	        });
	      }else{
	        this.spinnerService.hide();
	        this.toastr.error(response.msg);
	      }
    	});
  	};

}
 