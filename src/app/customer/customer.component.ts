import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.css"],
})
export class CustomerComponent implements OnInit {
  listing: any = [];
  customer: any = "";
  p: number[] = [];
  collection: any[];
  ii;
  constructor(
    public apiService: ApiService,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService,
    public router: Router,
    route: ActivatedRoute
  ) {
    this.getCustomersListing();
    const id: Observable<string> = route.params.pipe(
      map((listing) => listing._id)
    );
  }

  ngOnInit() {
    console.log("enter");
    this.getCustomersListing();
  }

  getCustomersListing() {
    this.spinnerService.show();
    this.apiService.all_users().subscribe(
      (response) => {
        console.log(response);
        this.spinnerService.hide();
        this.listing = response.data;
        this.collection = this.listing;
      },
      (err) => {
        this.toastr.error("Something went wrong.Please try later.");
      }
    );
  }

  viewDetails(index) {
    console.log("VIEW DETAIL", index);
    // localStorage.setItem("item", JSON.stringify(this.listing[index]));
    console.log(localStorage.getItem("item"));
    this.router.navigateByUrl("/customer-details/" + index);
  }

  editCustomer(index) {
    // localStorage.setItem("item", JSON.stringify(this.listing[index]));
    this.router.navigateByUrl("/edit-customer/" + index);
  }

  deletePopUpCustomer(row) {
    console.log("SELECTED FOR DELETE:", row);
    this.customer = row;
  }

  deleteCustomer() {
    this.spinnerService.show();
    let dict = {
      userid: this.customer._id,
    };

    this.apiService.user_delete(dict).subscribe((response) => {
      this.spinnerService.hide();
      this.getCustomersListing();
      this.toastr.success(response.msg);
    });
  }

  block_unblock(block_status, id) {
    this.spinnerService.show();
    var data = {
      block_status: block_status,
      userid: id,
    };
    this.apiService
      .influencer_user_block_unblock(data)
      .subscribe((response) => {
        this.spinnerService.hide();
        this.getCustomersListing();
      });
  }

  // 	editDetails(index){
  //     localStorage.setItem('item', JSON.stringify(this.listing[index]))
  //     this.router.navigateByUrl('/edit-customer');
  //   }
}
