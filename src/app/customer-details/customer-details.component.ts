import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";

import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from "@angular/router";

@Component({
  selector: "app-customer-details",
  templateUrl: "./customer-details.component.html",
  styleUrls: ["./customer-details.component.css"],
})
export class CustomerDetailsComponent implements OnInit {
  details: any;
  userInterests: any = "";
  public parameterValue: string;
  constructor(
    public apiService: ApiService,
    private router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    this.getDetails();
    //  this.userInterests = this.details.interests;
    //  console.log(this.details.paymentInfo);
    //  if (this.details.paymentInfo.length > 0) {
    //    for (var i = 0; i < this.details.paymentInfo.length; i++) {
    //      console.log(this.details.paymentInfo[i]);
    //      var num = this.details.paymentInfo[i].number;
    //      num = num.toString();
    //      this.details.paymentInfo[i].number = num.replace(/.(?=.{4,}$)/g, "X");
    //      console.log(this.details.paymentInfo[i].number);
    //      console.log(this.details);
    //    }
  }

  //  this.getInterestsAreas();
  // }

  // allInterests: any = "";
  // interestArr: any = [];
  // getInterestsAreas() {
  //   this.apiService.getAllInterests().subscribe((data) => {
  //     this.allInterests = data;
  //     if (this.userInterests.length > 0) {
  //       for (var i = 0; i < this.userInterests.length; i++) {
  //         if (this.userInterests[i].checked == true) {
  //           // console.log(this.userInterests[i]._id);
  //           for (var j = 0; j < this.allInterests.length; j++) {
  //             if (this.allInterests[j]._id == this.userInterests[i]._id) {
  //               this.allInterests[j].checked = true;
  //               this.interestArr.push({
  //                 interest: this.allInterests[j].interest,
  //                 _id: this.allInterests[j]._id,
  //                 checked: true,
  //               });
  //               //   console.log(this.allInterests);
  //               break;
  //             }
  //           }
  //         }
  //       }
  //     }
  //   });
  // }

  ngOnInit() {
    this.parameterValue = this._activatedRoute.snapshot.params.id;
  }
  getDetails() {
    this.parameterValue = this._activatedRoute.snapshot.params.id;

    const data = {
      uid: this.parameterValue,
    };

    console.log(data);
    this.apiService.getDetails(data).subscribe((res) => {
      console.log(res);
      this.details = res.data[0];
      console.log(this.details);
      console.log(this.details.firstName);
    });
  }
}
