import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService,
    public router: Router
  ) {}

  ngOnInit() {
    this.all_users();
    // this.getVendorsListing();
    this.all_levels();
    this.getAllQuestionsByUserType();
  }

  totalcustomers: any = "";
  totalvendors: any = "";
  all_Questions_Count: any = "";

  /********GET TOTAL CUSTOMERS **** */

  all_users() {
    this.spinnerService.show();
    this.apiService.all_users().subscribe(
      (response) => {
        console.log(response);
        this.spinnerService.hide();
        this.totalcustomers = response.data.length;
      },
      (err) => {
        this.toastr.error("Something went wrong.Please try later.");
      }
    );
  }

  /********GET TOTAL VENDORS **** */

  // getVendorsListing() {
  //   this.spinnerService.show();
  //   this.apiService.influencer_listing().subscribe(
  //     (response) => {
  //       console.log(response);
  //       this.spinnerService.hide();
  //       this.totalvendors = response.length;
  //     },
  //     (err) => {
  //       this.toastr.error("Something went wrong.Please try later.");
  //     }
  //   );
  // }

  /*********GET ALL TRANSACTIONS***** */

  all_transactionsCount: any = "";
  all_levels() {
    this.apiService.all_points().subscribe((response) => {
      this.all_transactionsCount = response.data.length;
      console.log(this.all_transactionsCount);
    });
  }

  /********GET ALL QUESTIONS COUNT***** */

  getAllQuestionsByUserType() {
    this.apiService.all_levels().subscribe((response) => {
      console.log(response);
      this.all_Questions_Count = response.data.length;
      console.log(this.all_Questions_Count);
    });
  }
}
