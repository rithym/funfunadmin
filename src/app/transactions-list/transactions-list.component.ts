import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
@Component({
  selector: "app-transactions-list",
  templateUrl: "./transactions-list.component.html",
  styleUrls: ["./transactions-list.component.css"],
})
export class TransactionsListComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    public toastr: ToastrService,
    public router: Router
  ) {}
  p: number = 1;
  collection: any[];
  ngOnInit() {
    this.all_levels();
  }

  all_transactions: any = "";
  deletesinglequestion;
  all_levels() {
    this.apiService.all_points().subscribe((response) => {
      this.all_transactions = response.data;
      console.log(response);
      this.collection = this.all_transactions;
    });

    // get_all_TransactionsHistory(){
    //   this.apiService.get_all_TransactionsHistory().subscribe((response) => {
    //     this.all_transactions = response;
    //     this.collection = this.all_transactions;
    //   })
    // }
  }
  _deleteSingleQuestion(qid) {
    this.deletesinglequestion = qid;
    console.log(this.deletesinglequestion);
  }
  /*  deleteData() {
    const data = {
      qid: this.deletesinglequestion,
    };
    console.log(data);
    this.apiService.delete_points(data).subscribe((response) => {
      console.log(response);
      this.all_levels();
      document.getElementById("closedeletemodal").click();
      this.toastr.success("Deleted successfully!");
    });
  }*/
}
