import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  profileData: any = { profile_image: null, firstName: "", lastName: "" };
  constructor(public router: Router) {
    if (localStorage.getItem("IsLoggedIn") == "true") {
      this.profileData = JSON.parse(localStorage.getItem("profile"));
    }
    console.log("here");
  }

  ngOnInit() {}

  logout() {
    localStorage.setItem("IsLoggedIn", "false");
    localStorage.setItem("profile", "null");
    this.router.navigateByUrl("/");
  }
}
