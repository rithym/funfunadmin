import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiService } from "../services/apiService";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  authForm: FormGroup;
  public loading = false;

  constructor(
    public fb: FormBuilder,
    public apiService: ApiService,
    public router: Router,
    private toastr: ToastrService,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.createForm();
    if (
      localStorage.getItem("isLoggedIn") != null &&
      localStorage.getItem("isLoggedIn") != undefined &&
      localStorage.getItem("isLoggedIn") != ""
    ) {
      this.router.navigateByUrl("/dashboard");
    } else {
    }
  }

  ngOnInit() {}

  createForm() {
    this.authForm = this.fb.group({
      email: ["", Validators.compose([Validators.required])],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(5)]),
      ],
    });
  }

  login() {
    this.spinnerService.show();
    console.log(this.authForm.value);
    var loginData = {
      email: this.authForm.value.email,
      password: this.authForm.value.password,
    };

    this.apiService.adminLogin(loginData).subscribe((response) => {
      console.log(response);
      // this.loading = false;
      this.spinnerService.hide();
      if (response.status == 1) {
        localStorage.setItem("adminId", response.data.Items[0].id);
        localStorage.setItem("IsLoggedIn", "true");
        localStorage.setItem("profile", JSON.stringify(response.data.Items[0]));
        if (
          this.authForm.value.email == "admin" &&
          this.authForm.value.password == "admin"
        ) {
          this.spinnerService.hide();
          localStorage.setItem("isLoggedIn", "true");
          this.router.navigateByUrl("/dashboard");
          this.toastr.success("Logged in successfully!");
        } else {
          this.spinnerService.hide();
          this.toastr.error("Wrong credentials!");
        }
      } else {
        this.toastr.error("Wrong credentials!");
      }
    });
  }
}
