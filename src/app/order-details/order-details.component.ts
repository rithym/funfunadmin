

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
	details: any;
  	constructor() {
      this.details = JSON.parse(localStorage.getItem('item'));
      console.log(this.details);
  	}

  	ngOnInit() {
  	}

}
