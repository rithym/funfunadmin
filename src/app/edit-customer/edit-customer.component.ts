import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Location } from "@angular/common";
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from "@angular/router";
import { CreditCardValidator } from "angular-cc-library";
@Component({
  selector: "app-edit-customer",
  templateUrl: "./edit-customer.component.html",
  styleUrls: ["./edit-customer.component.css"],
})
export class EditCustomerComponent implements OnInit {
  authForm: FormGroup;
  creditCardForm: FormGroup;
  editCreditCardForm: FormGroup;
  addNewForm: FormGroup;
  details: any = "";
  Image: any = null;
  selectedFile: any;

  submitted = false;
  cardid: any = "";

  name: any = "";
  email: any = "";
  userInterests: any = "";
  public parameterValue: string;
  constructor(
    public apiService: ApiService,
    public fb: FormBuilder,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService,
    private _location: Location,
    private router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    this.createForm();
    this.getDetails();
    //	  this.createCreditCardForm();
    this.createForm();
    //	  this.addNewCardForm();
    //this.details = JSON.parse(localStorage.getItem("item"));
    //	  this.userInterests = this.details.interests;
    //   if(this.details.paymentInfo.length>0){
    // 	  for(var i=0;i<this.details.paymentInfo.length;i++){
    // 			console.log(this.details.paymentInfo[i]);
    // 			var num = this.details.paymentInfo[i].number;
    // 			num = num.toString();
    // 		  	this.details.paymentInfo[i].number = num.replace(/.(?=.{4,}$)/g, 'X');
    // 			console.log(this.details.paymentInfo[i].number);
    // 			console.log(this.details);
    // 			console.log(JSON.parse(localStorage.getItem('item')).paymentInfo);
    // 			this.details.paymentInfo[i].card_number = JSON.parse(localStorage.getItem('item')).paymentInfo[i].number;
    // 			console.log(this.details);
    // 	  }
    //   }

    //	  this.getInterestsAreas();
  }

  ngOnInit() {
    // this.editCreditCardForm = this.fb.group({
    // 	cardName : ['',[Validators.required]],
    // 	number: ['', [Validators.required,<any>CreditCardValidator.validateCCNumber]],
    // 	expMonth: ['', [Validators.required ,<any>CreditCardValidator.validateExpDate]]
    // })
  }

  createForm() {
    this.parameterValue = this._activatedRoute.snapshot.params.id;

    const data = {
      uid: this.parameterValue,
    };

    console.log(data);
    this.apiService.getDetails(data).subscribe((res) => {
      console.log(res);
      this.details = res.data[0];
      console.log(this.details);
      console.log(this.details.firstName);
    });
    this.authForm = this.fb.group({
      firstName: [
        this.details.firstName,
        Validators.compose([Validators.required]),
      ],
      lastName: [
        this.details.lastName,
        Validators.compose([Validators.required]),
      ],
      username: [
        this.details.username,
        Validators.compose([Validators.required]),
      ],
      email: [
        this.details.email,
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"
          ),
        ]),
      ],
    });
  }

  // addNewCardForm(){
  // 	this.addNewForm = this.fb.group({
  // 		cardName : ['',[Validators.required]],
  // 		number: ['', [Validators.required,<any>CreditCardValidator.validateCCNumber]],
  // 		expMonth: ['', [Validators.required ,<any>CreditCardValidator.validateExpDate]]
  //     });
  // }

  // createCreditCardForm(){

  // 	this.creditCardForm = this.fb.group({
  // 	  cardname : ['',[Validators.required]],
  // 	  cardnumber: ['', [Validators.required]],
  // 	  expMonth: ['', [Validators.required]],
  // 	  expYear: ['', [Validators.required]],
  // 	  cvc: ['', [Validators.required]]
  // 	},{});

  // }

  // updateInterestAreas(){
  // 	var data = {
  // 		userid : this.details._id,
  // 		interests : this.interestArr
  // 	}
  // 	console.log(data);
  // 	this.apiService.updateUserInterests(data).subscribe((res)=>{
  // 	  console.log(res);
  // 	})
  // }

  updateCustomer() {
    this.spinnerService.show();
    let dict = this.authForm.value;
    dict.userid = this.details._id;
    console.log("FORM VALUES:", dict);
    this.apiService.update_user(dict).subscribe((response) => {
      this.spinnerService.hide();
      if (response.status === true) {
        console.log(response);
        this._location.back();
        //this.navCtrl.dismiss();
        //	this.updateInterestAreas();
        this.toastr.success(response.msg);
      } else {
        this.toastr.error(response.msg);
      }
    });
  }
  getDetails() {
    this.parameterValue = this._activatedRoute.snapshot.params.id;

    const data = {
      uid: this.parameterValue,
    };

    console.log(data);
    this.apiService.getDetails(data).subscribe((res) => {
      console.log(res);
      this.details = res.data[0];
      console.log(this.details);
      console.log(this.details.firstName);
    });
  }
  backView() {
    this._location.back();
  }

  // _deletecarddetails:any='';
  // deleteCard(cardid){

  // 	const data = {

  // 		cardid : cardid,
  // 		userid: this.details._id

  // 	}

  // 	this._deletecarddetails = data;
  // 	console.log(this._deletecarddetails);
  // 	console.log(this.details);
  // 	console.log(JSON.parse(localStorage.getItem('item')));

  // }

  // _deleteCard(){
  // 	console.log("delete");
  // 	document.getElementById('closepop').click();
  // 	this.apiService.delete_payment_Details(this._deletecarddetails).subscribe((res)=>{
  // 		console.log(res);
  // 		this.toastr.success("Card Deleted!");
  // 		for(var j=0;j<this.details.paymentInfo.length;j++){
  // 			if(this.details.paymentInfo[j].id == this._deletecarddetails.cardid){
  // 				this.details.paymentInfo.splice(this.details.paymentInfo.indexOf(this.details.paymentInfo[j]),1);
  // 				console.log(this.details);
  // 			}
  // 		}
  // 	})

  // }

  // allInterests:any='';
  // interestArr:any=[];
  // getInterestsAreas(){

  //   this.apiService.getAllInterests().subscribe((data)=>{
  // 	this.allInterests = data;
  // 	if(this.userInterests.length>0){
  // 		  for(var i=0;i<this.userInterests.length;i++){
  // 			if(this.userInterests[i].checked == true){
  // 			  for(var j=0;j<this.allInterests.length;j++){
  // 				  if(this.allInterests[j]._id == this.userInterests[i]._id){
  // 					this.allInterests[j].checked  =  true;
  // 					this.interestArr.push({
  // 					  'interest' : this.allInterests[j].interest,
  // 					  '_id' : this.allInterests[j]._id,
  // 					  'checked' : true
  // 					})
  // 					break;
  // 				  }
  // 			  }
  // 			}
  // 	  }
  // 	}
  //   })

  // }

  // /*************SELECT INTERESTS*************** */

  // selectInterests(interest,id){
  // 	console.log(interest);
  // 	console.log(id);
  // 	console.log(this.interestArr);
  // 	if(this.interestArr.length>0){
  // 	  for(var i=0;i<this.interestArr.length;i++){
  // 		  if(interest == this.interestArr[i].interest){
  // 			this.interestArr.splice(i,1);
  // 			console.log(this.interestArr);
  // 			break;
  // 		  }else if(interest != this.interestArr[i].interest && i==this.interestArr.length-1){
  // 			this.interestArr.push({
  // 			  'interest' : interest,
  // 			  '_id' : id,
  // 			  'checked' : true
  // 			})
  // 			console.log(this.interestArr);
  // 			break;
  // 		  }
  // 	  }
  // 	}else{
  // 		  this.interestArr.push({
  // 			'interest' : interest,
  // 			'_id' : id,
  // 			'checked' : true
  // 		  })
  // 		  console.log(this.interestArr);
  // 	}

  // }

  // randomString() {
  // 	let charSet = "";
  // 	let len = 10;
  // 	charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  // 	var randomString = '';
  // 	for (var i = 0; i < len; i++) {
  // 		var randomPoz = Math.floor(Math.random() * charSet.length);
  // 		randomString += charSet.substring(randomPoz,randomPoz+1);
  // 		console.log(randomString);
  // 	}
  // 	return randomString;
  // }

  // getCreditCardType(accountNumber)
  // {

  // 		//start without knowing the credit card type
  // 		var result = "unknown";

  // 		//first check for MasterCard
  // 		if (/^5[1-5]/.test(accountNumber))
  // 		{
  // 			result = "mastercard";
  // 		}

  // 		//then check for Visa
  // 		else if (/^4/.test(accountNumber))
  // 		{
  // 			result = "visa";
  // 		}

  // 		//then check for AmEx
  // 		else if (/^3[47]/.test(accountNumber))
  // 		{
  // 			result = "amex";
  // 		}
  // 		else if (/^6(?:011|5[0-9]{2})[0-9]{12}/.test(accountNumber))
  // 		{
  // 			result = "Discover";
  // 		}
  // 		else if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}/.test(accountNumber))
  // 		{
  // 			result = "Diners Club";
  // 		}
  // 		else if (/^(?:2131|1800|35\d{3})\d{11}/.test(accountNumber))
  // 		{
  // 			result = "JCB";
  // 		}
  // 		return result;
  // }

  // _saveCardDetails(form){
  // 	console.log(form.value);
  // 	var data = {
  // 	  userid : this.details._id,
  // 	  card : {
  // 		id : this.randomString(),
  // 		name : form.value.cardName,
  // 		number: form.value.number,
  // 		expMonth: form.value.expMonth.split('/')[0],
  // 		expYear: form.value.expMonth.split('/')[1],
  // 		cvc: null,
  // 		cardtype : this.getCreditCardType(form.value.number),
  // 		card_number : form.value.number.replace(/.(?=.{4,}$)/g, 'X')
  // 	  }
  // 	}

  // 	var newCard = [];
  // 	newCard.push(data.card);
  // 	console.log(newCard);

  // 	console.log("SAVE PAYMENT DATA");
  // 	this.apiService.updatePaymentDetails(data).subscribe((res)=>{
  // 		console.log(res);
  // 		this.toastr.success('Card Added Successfully!');
  // 		document.getElementById('closemodal').click();
  // 		var newPaymentArr = [];
  // 		var userDetails  = JSON.parse(localStorage.getItem('item'));
  // 		console.log(userDetails);
  // 		if(userDetails.paymentInfo.length > 0){
  // 			for(var j=0;j<userDetails.paymentInfo.length;j++){
  // 				newPaymentArr.push(userDetails.paymentInfo[j]);
  // 				console.log(newPaymentArr);
  // 			}

  // 				var newArr = newPaymentArr.concat(newCard);
  // 				console.log(newArr);
  // 				userDetails.paymentInfo = newArr;
  // 				console.log(userDetails);
  // 				this.details = userDetails;
  // 				localStorage.setItem('item',JSON.stringify(this.details));
  // 				// if(this.details.paymentInfo.length>0){
  // 				// 	for(var i=0;i<this.details.paymentInfo.length;i++){
  // 				// 		  console.log(this.details.paymentInfo[i]);
  // 				// 		  var num = this.details.paymentInfo[i].number;
  // 				// 		  num = num.toString();
  // 				// 			this.details.paymentInfo[i].number = num.replace(/.(?=.{4,}$)/g, 'X');
  // 				// 		  console.log(this.details.paymentInfo[i].number);
  // 				// 		  console.log(this.details);
  // 				// 		  console.log(JSON.parse(localStorage.getItem('item')).paymentInfo);
  // 				// 		  this.details.paymentInfo[i].card_number = JSON.parse(localStorage.getItem('item')).paymentInfo[i].number;
  // 				// 		  console.log(this.details);
  // 				// 	}
  // 				// }
  // 		}else{
  // 			userDetails.paymentInfo = newCard;
  // 			console.log(userDetails);
  // 			this.details = userDetails;
  // 			localStorage.setItem('item',JSON.stringify(this.details));
  // 			// if(this.details.paymentInfo.length>0){
  // 			// 	for(var i=0;i<this.details.paymentInfo.length;i++){
  // 			// 		  console.log(this.details.paymentInfo[i]);
  // 			// 		  var num = this.details.paymentInfo[i].number;
  // 			// 		  num = num.toString();
  // 			// 			this.details.paymentInfo[i].number = num.replace(/.(?=.{4,}$)/g, 'X');
  // 			// 		  console.log(this.details.paymentInfo[i].number);
  // 			// 		  console.log(this.details);
  // 			// 		  console.log(JSON.parse(localStorage.getItem('item')).paymentInfo);
  // 			// 		  this.details.paymentInfo[i].card_number = JSON.parse(localStorage.getItem('item')).paymentInfo[i].number;
  // 			// 		  console.log(this.details);
  // 			// 	}
  // 			// }
  // 		}

  // 	},err=>{
  // 		this.toastr.success('Unable to Add Card! Try Again');
  // 	})
  // }
  // editcardid : any = '';
  // _getCardDetails(paymentdetails){
  // 	console.log(paymentdetails);
  // 	this.editcardid = paymentdetails.id;
  // 	this.editCreditCardForm = this.fb.group({
  // 		cardName : [paymentdetails.name,[Validators.required]],
  // 		number: [paymentdetails.number, [Validators.required,<any>CreditCardValidator.validateCCNumber]],
  // 		expMonth: [paymentdetails.expMonth + '/' + paymentdetails.expYear, [Validators.required ,<any>CreditCardValidator.validateExpDate]]
  //   	})
  // }

  // editCustomerCard(form){

  // 	console.log(form);
  // 	var paymentdata = {
  // 		id : this.editcardid,
  // 		name : form.value.cardName,
  // 		number: form.value.number,
  // 		expMonth: form.value.expMonth.split('/')[0],
  // 		expYear: form.value.expMonth.split('/')[1],
  // 		cvc: null,
  // 		cardtype : this.getCreditCardType(form.value.number),
  // 		card_number : form.value.number.replace(/.(?=.{4,}$)/g, 'X')
  // 	}

  // 	var paymentArr = [];
  // 	var userDetails = this.details;
  // 	for(var i = 0 ;i < userDetails.paymentInfo.length;i++){
  // 			console.log(userDetails.paymentInfo);
  // 			if(this.editcardid == userDetails.paymentInfo[i].id){
  // 				userDetails.paymentInfo[i] = paymentdata;
  // 				console.log(userDetails);
  // 			}
  // 			paymentArr.push(userDetails.paymentInfo[i]);
  // 			console.log(paymentArr);
  // 	}
  // 	var data = {
  // 		userid : this.details._id,
  // 		cards : paymentArr
  // 	  }

  // 	  console.log("SAVE PAYMENT DATA");
  // 	  this.apiService.editPaymentDetails(data).subscribe((res)=>{
  // 		  console.log(res);
  // 		  this.toastr.success('Card Updated Successfully!');
  // 		  document.getElementById('closemodal2').click();
  // 	  },err=>{
  // 		  this.toastr.success('Unable to Add Card! Try Again');
  // 	  })
  // }

  // _addCardForm(){
  // 	this.addNewForm = this.fb.group({
  // 		cardName : ['',[Validators.required]],
  // 		number: ['', [Validators.required,<any>CreditCardValidator.validateCCNumber]],
  // 		expMonth: ['', [Validators.required ,<any>CreditCardValidator.validateExpDate]]
  //     });
  // }
}
