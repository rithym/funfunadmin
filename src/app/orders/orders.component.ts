import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"],
})
export class OrdersComponent implements OnInit {
  deletesinglequestion: any = "";
  constructor(
    public apiService: ApiService,
    public toastr: ToastrService,
    public router: Router
  ) {}

  ngOnInit() {
    this.getAllQuestionsByUserType();
  }
  // p: number = 1;
  p: number[] = [];
  collection: any[];
  all_Questions: any = "";
  fa;
  getAllQuestionsByUserType() {
    this.apiService.all_levels().subscribe((response) => {
      console.log(response);
      this.all_Questions = response.data;

      this.fa = Infinity;

      this.collection = this.all_Questions;
    });
  }

  _deleteSingleQuestion(qid) {
    this.deletesinglequestion = qid;
  }

  _deleteQuestion() {
    const data = {
      qid: this.deletesinglequestion,
    };
    console.log(data);
    this.apiService.delete_level(data).subscribe((response) => {
      console.log(response);
      this.getAllQuestionsByUserType();
      document.getElementById("closedeletemodal").click();
      this.toastr.success("Deleted successfully!");
    });
  }

  viewDetails(index) {
    localStorage.setItem("item", JSON.stringify(this.all_Questions[index]));
    this.router.navigateByUrl("/order-details");
  }
}
