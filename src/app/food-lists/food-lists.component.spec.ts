import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodListsComponent } from './food-lists.component';

describe('FoodListsComponent', () => {
  let component: FoodListsComponent;
  let fixture: ComponentFixture<FoodListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
