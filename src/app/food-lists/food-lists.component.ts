import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
@Component({
  selector: "app-food-lists",
  templateUrl: "./food-lists.component.html",
  styleUrls: ["./food-lists.component.css"],
})
export class FoodListsComponent implements OnInit {
  listing: any = [];
  vendor: any;
  p: number[] = [];
  collection: any[];
  constructor(
    public apiService: ApiService,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService,
    public router: Router
  ) {
    this.terms_listing();
  }

  ngOnInit() {}
  terms_listing() {
    this.spinnerService.show();
    this.apiService.terms_listing().subscribe(
      (response) => {
        console.log(response);
        this.spinnerService.hide();
        this.listing = response.data;
      },
      (err) => {
        this.toastr.error("Something went wrong.Please try later.");
      }
    );
  }

  checkIsOpenOrClosed() {}

  viewDetails(index) {
    localStorage.setItem("item", JSON.stringify(this.listing[index]));
    this.router.navigateByUrl("/vendor-details");
  }

  editDetails(index) {
    localStorage.setItem("item", JSON.stringify(this.listing[index]));
    this.router.navigateByUrl("/edit-vendor");
  }

  deletePopUpVendor(row) {
    this.vendor = row;
  }

  deleteVendor() {
    this.spinnerService.show();
    let dict = {
      userid: this.vendor._id,
    };

    this.apiService.deleteVendor(dict).subscribe((response) => {
      this.spinnerService.hide();
      this.terms_listing();
      this.toastr.success(response.msg);
    });
  }

  approveDisapproveAccount(approval, userid) {
    console.log(approval);
    this.spinnerService.show();

    var data = {
      approval: approval,
      userid: userid,
    };
    this.apiService.approve_disapprove(data).subscribe(
      (res) => {
        this.spinnerService.hide();
        if (approval == "0") {
          this.toastr.success("Account has been Approved");
          this.terms_listing();
        } else if (approval == "2") {
          this.toastr.success("Account has been Disaaproved");
          this.terms_listing();
        }
      },
      (err) => {
        this.toastr.error("Sorry , Try Again!");
      }
    );
  }

  block_unblock(block_status, id) {
    this.spinnerService.show();
    var data = {
      block_status: block_status,
      userid: id,
    };
    this.apiService
      .influencer_user_block_unblock(data)
      .subscribe((response) => {
        this.spinnerService.hide();
        this.terms_listing();
      });
  }
}
