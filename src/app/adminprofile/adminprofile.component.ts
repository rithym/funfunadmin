import { Component, OnInit } from "@angular/core";
import { ApiService } from "../services/apiService";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-adminprofile",
  templateUrl: "./adminprofile.component.html",
  styleUrls: ["./adminprofile.component.css"],
})
export class AdminprofileComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    public fb: FormBuilder,
    public spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastrService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  profileData: any = JSON.parse(localStorage.getItem("profile"));
  authForm: FormGroup;
  authForm1: FormGroup;
  Image: any = null;
  selectedFile: any;

  createForm() {
    console.log(this.profileData);
    this.authForm = this.fb.group({
      fname: [
        this.profileData.fname,
        Validators.compose([Validators.required]),
      ],
      lname: [
        this.profileData.lname,
        Validators.compose([Validators.required]),
      ],
      email: [
        this.profileData.email,
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"
          ),
        ]),
      ],
      contact: [
        this.profileData.contact,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]{4,15}"),
        ]),
      ],
      address: [
        this.profileData.address,
        Validators.compose([Validators.required]),
      ],
      city: [
        this.profileData.city,
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z -']+"),
        ]),
      ],
      country: [
        this.profileData.country,
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z -']+"),
        ]),
      ],
      profile_image: [this.profileData.profile_image],
    });

    this.authForm1 = this.fb.group({
      password: ["", Validators.compose([Validators.required])],
      newPassword: [
        "",
        Validators.compose([Validators.required, Validators.minLength(5)]),
      ],
      confirmPassword: [
        "",
        Validators.compose([Validators.required, Validators.minLength(5)]),
      ],
    });
  }

  updatePassword() {
    var passwordData = {
      id: this.profileData.id,
      password: this.authForm1.value.newPassword,
    };
    if (this.authForm1.value.password == "") {
    } else if (this.authForm1.value.newPassword == "") {
    } else if (this.authForm1.value.confirmPassword == "") {
    } else if (
      this.authForm1.value.newPassword != this.authForm1.value.confirmPassword
    ) {
      alert("Your new password and confirm password does not match!");
    } else if (this.authForm1.value.password != this.profileData.password) {
      alert("Your current password is incorrect!");
    } else {
      this.spinnerService.show();
      this.apiService.updatePassword(passwordData).subscribe((response) => {
        console.log(response);
        this.profileData.password = this.authForm1.value.newPassword;
        localStorage.setItem("profile", JSON.stringify(this.profileData));
        this.toastr.success("Your password updated successfully!");
        this.spinnerService.hide();
      });
    }
  }

  updateProfile() {
    this.spinnerService.show();
    if (this.Image == null) {
      var profileData = {
        id: this.profileData.id,
        address: this.authForm.value.address,
        city: this.authForm.value.city,
        contact: this.authForm.value.contact,
        country: this.authForm.value.country,
        email: this.authForm.value.email,
        firstName: this.authForm.value.firstName,
        lname: this.authForm.value.lname,
        profile_image: this.authForm.value.profile_image,
        password: this.profileData.password,
      };
      this.apiService.UpdateProfile(profileData).subscribe((response) => {
        localStorage.setItem("profile", JSON.stringify(profileData));
        this.profileData = this.authForm.value;
        this.toastr.success("Profile updated successfully!");
        this.spinnerService.hide();
      });
    } else {
      //upload image
      this.uploadImage();
    }
  }

  onImageChanged(event) {
    let reader = new FileReader();
    this.Image = event.target.files[0];
    this.selectedFile = event.target.files[0];
    let extn = event.target.value.split(".")[1];
    if (extn == "jpg" || extn == "png" || extn == "jpeg") {
      reader.onload = () => {
        this.Image = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    } else if (extn == "pdf") {
      this.Image = event.target.files[0].name;
    }
  }

  uploadImage() {
    this.spinnerService.show();

    var payload = new FormData();
    payload.append("image", this.selectedFile, this.selectedFile.name);
    this.apiService.UploadImage(payload).subscribe((response) => {
      console.log(response);
      this.authForm.value.profile_image = response.image;
      console.log(this.authForm.value);
      this.Image = null;
      this.profileData.profile_image = response.image;
      this.updateProfile();
    });
  }
  /* add_admin() {
    this.apiService.add_admin().subscribe((response) => {
      console.log(response);
    });
  }*/
}
