import { Http, URLSearchParams } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/Rx";

@Injectable()
export class ApiService {
  constructor(private http: Http) {}

  loggedIn(data) {
    return this.http.post("admin_login", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*************ALL USERS LISTING*********** */
  all_users() {
    return this.http.get("api/v1/auth/all_users").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*************PRIVACY LISTING*********** */
  privacy_listing() {
    return this.http.get("api/v1/auth/privacy_listing").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*************TERMS LISTING*********** */
  terms_listing() {
    return this.http.get("api/v1/auth/terms_listing").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /**********APPROVE/Disapprove Influencer ACCOUNT******* */

  approve_disapprove(data) {
    return this.http
      .post("api/v1/admin/pimm/influencer_account_approval_status", data)
      .map(
        (data) => {
          return data.json();
        },
        (error) => {
          return error.json();
        }
      );
  }

  /*********BLOCK/UNBLOCK USER***************** */

  influencer_user_block_unblock(data) {
    return this.http
      .post("api/v1/admin/pimm/influencer_user_block_unblock", data)
      .map(
        (data) => {
          return data.json();
        },
        (error) => {
          return error.json();
        }
      );
  }

  /**********DELETE USER******* */

  user_delete(data) {
    return this.http.post("api/v1/auth/user_delete", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /**********update_customer_influencer***** */

  update_customer_influencer(data) {
    return this.http
      .post("api/v1/admin/pimm/update_customer_influencer", data)
      .map(
        (data) => {
          return data.json();
        },
        (error) => {
          return error.json();
        }
      );
  }

  /***********UPLOAD INFLUENCER IMAGE*********** */

  UploadImage(data) {
    return this.http.post("api/v1/admin/pimm/UploadImage", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /***********UPDATE INFLUENCER IMAGE********** */

  UpdateInfluencerImage(data) {
    return this.http.post("api/v1/admin/pimm/UpdateInfluencerImage", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*********UPDATE PAYMENT DETAILS*********** */

  updatePaymentDetails(data) {
    return this.http.post("api/v1/user/pimm/update_payment_Details", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*********EDIT PAYMENT DETAILS*********** */

  editPaymentDetails(data) {
    return this.http.post("api/v1/user/pimm/edit_payment_Details", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /*********DELETE PAYMENT DETAILS*********** */
  delete_payment_Details(data) {
    return this.http.post("api/v1/user/pimm/delete_payment_Details", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /***********ADD CARD********** */
  create_card(data) {
    return this.http.post("/api/v1/user/pimm/update_payment_Details", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /***********ADD CARD********** */
  all_points() {
    return this.http.get("api/v1/auth/all_points").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  /***********ADD CARD********** */
  all_levels() {
    return this.http.get("api/v1/auth/all_levels").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  add_admin() {
    return this.http.get("api/v1/auth/add_admin").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  delete_level(data) {
    return this.http.post("api/v1/auth/delete_level", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  delete_points(data) {
    return this.http.post("api/v1/auth/delete_points", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  getAllInterests() {
    return this.http.get("api/v1/user/pimm/get_all_Interests").map((res) => {
      return res.json();
    });
  }

  add_user(data) {
    console.log(data);
    return this.http.post("api/v1/auth/add_user", data).map((res) => {
      return res.json();
    });
  }
  add_points(data) {
    console.log(data);
    return this.http.post("api/v1/auth/add_points", data).map((res) => {
      return res.json();
    });
  }

  diff_levels(data) {
    console.log(data);
    return this.http.post("api/v1/auth/diff_levels", data).map((res) => {
      return res.json();
    });
  }

  updateUserInterests(data) {
    return this.http
      .post("api/v1/user/pimm/update_Interests", data)
      .map((res) => {
        return res.json();
      });
  }

  update_user(data) {
    return this.http.post("api/v1/auth/update_user", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  deleteCustomer(data) {
    return this.http.post("admin/delete_customer", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  add_vendor(data) {
    return this.http.post("admin/add_vendor", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  vendor_listing() {
    return this.http.get("admin/vendor_listing").map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  update_vendor(data) {
    return this.http.post("admin/update_vendor", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  deleteVendor(data) {
    return this.http.post("admin/delete_vendor", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  is_email_exist(data) {
    return this.http.post("admin/is_email_exist", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  // UploadImage(data){
  //   return this.http.post('upload_image', data).map((data)=>{
  //     return data.json();
  //   }, error => {
  //     return error.json();
  //   });
  // }

  adminLogin(data) {
    return this.http.post("admin/admin_login", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  updatePassword(data) {
    return this.http.post("admin/admin_password_update", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  UpdateProfile(data) {
    return this.http.post("api/v1/auth/UpdateProfile", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }

  getDetails(data) {
    return this.http.post("api/v1/auth/getDetails", data).map(
      (data) => {
        return data.json();
      },
      (error) => {
        return error.json();
      }
    );
  }
}
