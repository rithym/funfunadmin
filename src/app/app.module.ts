import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { HeaderComponent } from "./header/header.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { FooterComponent } from "./footer/footer.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ProfileComponent } from "./profile/profile.component";
import { LoginComponent } from "./login/login.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { CustomerComponent } from "./customer/customer.component";
import { CustomerDetailsComponent } from "./customer-details/customer-details.component";
import { EditCustomerComponent } from "./edit-customer/edit-customer.component";
import { AddCustomerComponent } from "./add-customer/add-customer.component";
import { VendorsComponent } from "./vendors/vendors.component";
import { OrdersComponent } from "./orders/orders.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { FoodMenuComponent } from "./food-menu/food-menu.component";
import { CustomerOrdersComponent } from "./customer-orders/customer-orders.component";
import { OrderDetailsComponent } from "./order-details/order-details.component";
import { CustomerFeedbackComponent } from "./customer-feedback/customer-feedback.component";
import { FavoritesComponent } from "./favorites/favorites.component";
import { VendorDetailsComponent } from "./vendor-details/vendor-details.component";
import { AddVendorComponent } from "./add-vendor/add-vendor.component";
import { EditVendorComponent } from "./edit-vendor/edit-vendor.component";
import { VendorOrdersComponent } from "./vendor-orders/vendor-orders.component";
import { VendorFeedbackComponent } from "./vendor-feedback/vendor-feedback.component";
import { VendorFoodsComponent } from "./vendor-foods/vendor-foods.component";
import { FoodDetailsComponent } from "./food-details/food-details.component";
import { FoodListsComponent } from "./food-lists/food-lists.component";
import { VendorFoodDetailsComponent } from "./vendor-food-details/vendor-food-details.component";
import { ReportsComponent } from "./reports/reports.component";

import { HttpModule, RequestOptions } from "@angular/http";
import { CustomRequestOptions } from "./services/apilink";
import { ApiService } from "./services/apiService";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastrModule } from "ngx-toastr";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AdminprofileComponent } from "./adminprofile/adminprofile.component";
import { TransactionsListComponent } from "./transactions-list/transactions-list.component";
import { NgxPaginationModule } from "ngx-pagination"; // <-- import the module
import { NgxStripeModule } from "ngx-stripe";
import { CreditCardDirectivesModule } from "angular-cc-library";
const routes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "profile", component: ProfileComponent },
  { path: "adminprofile", component: AdminprofileComponent },
  { path: "", component: LoginComponent },
  { path: "forgot-password", component: ForgotPasswordComponent },
  { path: "user", component: CustomerComponent },
  { path: "customer-details/:id", component: CustomerDetailsComponent },
  { path: "edit-customer/:id", component: EditCustomerComponent },
  { path: "add-customer", component: AddCustomerComponent },
  { path: "vendors", component: VendorsComponent },
  { path: "questions-answers", component: OrdersComponent },
  { path: "notifications", component: NotificationsComponent },
  { path: "food-menu", component: FoodMenuComponent },
  { path: "customer-orders", component: CustomerOrdersComponent },
  { path: "order-details", component: OrderDetailsComponent },
  { path: "customer-feedback", component: CustomerFeedbackComponent },
  { path: "favorites", component: FavoritesComponent },
  { path: "vendor-details", component: VendorDetailsComponent },
  { path: "add-vendor", component: AddVendorComponent },
  { path: "edit-vendor", component: EditVendorComponent },
  { path: "vendor-orders", component: VendorOrdersComponent },
  { path: "vendor-feedback", component: VendorFeedbackComponent },
  { path: "vendor-foods", component: VendorFoodsComponent },
  { path: "food-details", component: FoodDetailsComponent },
  { path: "food-lists", component: FoodListsComponent },
  { path: "vendor-food-details", component: VendorFoodDetailsComponent },
  { path: "reports", component: ReportsComponent },
  { path: "transactions", component: TransactionsListComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    ProfileComponent,
    LoginComponent,
    ForgotPasswordComponent,
    CustomerComponent,
    CustomerDetailsComponent,
    EditCustomerComponent,
    AddCustomerComponent,
    VendorsComponent,
    OrdersComponent,
    NotificationsComponent,
    FoodMenuComponent,
    CustomerOrdersComponent,
    OrderDetailsComponent,
    CustomerFeedbackComponent,
    FavoritesComponent,
    VendorDetailsComponent,
    AddVendorComponent,
    EditVendorComponent,
    VendorOrdersComponent,
    VendorFeedbackComponent,
    VendorFoodsComponent,
    FoodDetailsComponent,
    FoodListsComponent,
    VendorFoodDetailsComponent,
    ReportsComponent,
    AdminprofileComponent,
    TransactionsListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    RouterModule.forRoot(routes, { useHash: true }),
    NgbModule,
    NgxPaginationModule,
    CreditCardDirectivesModule,
    NgxStripeModule.forRoot("pk_test_8ghnSkc1YDWnhEuVkhyhHNIc00qj96jauv"),
  ],
  providers: [
    ApiService,
    { provide: RequestOptions, useClass: CustomRequestOptions },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
