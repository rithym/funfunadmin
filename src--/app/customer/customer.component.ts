import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer', 
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  	listing: any = [];
  	customer: any = '';
	constructor(public apiService: ApiService, public spinnerService: 			Ng4LoadingSpinnerService, public toastr: ToastrService, public router: Router) {
		this.getCustomersListing();
	}



	

	ngOnInit() {
	}

	getCustomersListing(){
		this.spinnerService.show();
		this.apiService.customer_listing().subscribe((response) => {
				console.log(response);
				this.spinnerService.hide();
				this.listing = response;
			}, err => {
				this.toastr.error('Something went wrong.Please try later.');
		});
	}

	viewDetails(index){
		localStorage.setItem('item', JSON.stringify(this.listing[index]))
		this.router.navigateByUrl('/customer-details');
	}

	editCustomer(index){
		localStorage.setItem('item', JSON.stringify(this.listing[index]))
		this.router.navigateByUrl('/edit-customer');
	}

	deletePopUpCustomer(row){
		console.log(row);
    	this.customer = row;
	}
	  
  	deleteCustomer(){
	    this.spinnerService.show();
	    let dict = {
	      userid: this.customer._id
	    };

	    this.apiService.influencer_user_delete(dict).subscribe((response) => {
	 
	      this.spinnerService.hide();
	      this.getCustomersListing();  
	      this.toastr.success(response.msg);
    	}); 
	};
		
	block_unblock(block_status,id){
			this.spinnerService.show();
			var data = {
				block_status : block_status,
				userid : id
			}
			this.apiService.influencer_user_block_unblock(data).subscribe((response)=>{
				this.spinnerService.hide();
				this.getCustomersListing(); 
			})
	}

  // 	editDetails(index){
  //     localStorage.setItem('item', JSON.stringify(this.listing[index]))
  //     this.router.navigateByUrl('/edit-customer');
  //   }


}
