import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.css']
})
export class VendorsComponent implements OnInit {
	listing: any = [];
  vendor: any;
  constructor(public apiService: ApiService, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, public router: Router) {
  		this.getVendorsListing();
	}

  	ngOnInit() {
  	}

  	getVendorsListing(){
  		this.spinnerService.show();
		  this.apiService.influencer_listing().subscribe((response) => {
  			console.log(response)
  			this.spinnerService.hide();
  			this.listing = response;
  		},err=>{
				this.toastr.error('Something went wrong.Please try later.');
			});
  	}

  	checkIsOpenOrClosed(){

  	}

  	viewDetails(index){
  		localStorage.setItem('item', JSON.stringify(this.listing[index]))
  		this.router.navigateByUrl('/vendor-details');
  	}

    editDetails(index){
      localStorage.setItem('item', JSON.stringify(this.listing[index]))
      this.router.navigateByUrl('/edit-vendor');
    }

    deletePopUpVendor(row){
      this.vendor = row;
    }

    deleteVendor(){
      this.spinnerService.show();
      let dict = {
        userid: this.vendor._id
      };

      this.apiService.deleteVendor(dict).subscribe((response) => {
   
        this.spinnerService.hide();
        this.getVendorsListing();  
        this.toastr.success(response.msg);
      }); 
    };

    approveDisapproveAccount(approval,userid){
        console.log(approval);
        this.spinnerService.show();

        var data = {
          approval  : approval,
          userid : userid
        }
        this.apiService.approve_disapprove(data).subscribe((res)=>{

          this.spinnerService.hide();
          if(approval == '0'){
            this.toastr.success("Account has been Approved");
            this.getVendorsListing();
          }else if(approval == '2'){
            this.toastr.success("Account has been Disaaproved");
            this.getVendorsListing();
          }
          
        },err=>{
          this.toastr.error("Sorry , Try Again!");
        })
    }

    block_unblock(block_status,id){
			this.spinnerService.show();
			var data = {
				block_status : block_status,
				userid : id
			}
			this.apiService.influencer_user_block_unblock(data).subscribe((response)=>{
				this.spinnerService.hide();
				this.getVendorsListing(); 
			})
		}

}
