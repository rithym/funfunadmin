import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/apiService';
@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {

    constructor(public apiService: ApiService) { }

    ngOnInit() {
      this.get_all_TransactionsHistory();
    }

    all_transactions : any = '';
    get_all_TransactionsHistory(){
      this.apiService.get_all_TransactionsHistory().subscribe((response) => {
        this.all_transactions = response;
      })
    }

}
