import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public apiService: ApiService, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, public router: Router) { }

  ngOnInit() {

    this.getCustomersListing();
    this.getVendorsListing();
    this.get_all_TransactionsCount();
    this.getAllQuestionsCount();

  }

  totalcustomers:any='';
  totalvendors:any='';
  all_Questions_Count:any='';

  /********GET TOTAL CUSTOMERS **** */

  getCustomersListing(){
		this.spinnerService.show();
		this.apiService.customer_listing().subscribe((response) => {
				console.log(response);
				this.spinnerService.hide();
				this.totalcustomers = response.length;
			}, err => {
				this.toastr.error('Something went wrong.Please try later.');
		});
  }
  
  /********GET TOTAL VENDORS **** */

  getVendorsListing(){
    this.spinnerService.show();
    this.apiService.influencer_listing().subscribe((response) => {
      console.log(response);
      this.spinnerService.hide();
      this.totalvendors = response.length;
    },err=>{
      this.toastr.error('Something went wrong.Please try later.');
    });
  }

  /*********GET ALL TRANSACTIONS***** */

  all_transactionsCount : any = '';
  get_all_TransactionsCount(){
    this.apiService.get_all_TransactionsHistory().subscribe((response) => {
      this.all_transactionsCount = response.length;
    })
  }

  /********GET ALL QUESTIONS COUNT***** */

  getAllQuestionsCount(){
    this.apiService.get_all_questions_by_usertype().subscribe((response) => {
      console.log(response);
      this.all_Questions_Count = response.length;
    })
  }
}
