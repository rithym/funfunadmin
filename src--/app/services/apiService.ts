import {Http,URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/Rx';

@Injectable()
export class ApiService{
  constructor(private http:Http){}

  loggedIn(data) {
    return this.http.post('admin_login', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  };

  /*************ALL USERS LISTING*********** */
  customer_listing(){
    return this.http.get('api/v1/admin/pimm/get_all_users').map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }


  /*************ALL Influencers LISTING*********** */
  influencer_listing(){
    return this.http.get('api/v1/admin/pimm/get_all_influencers').map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /**********APPROVE/Disapprove Influencer ACCOUNT******* */
  
  approve_disapprove(data){
    return this.http.post('api/v1/admin/pimm/influencer_account_approval_status', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /*********BLOCK/UNBLOCK USER***************** */

  influencer_user_block_unblock(data){
    return this.http.post('api/v1/admin/pimm/influencer_user_block_unblock', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /**********DELETE INFLUENCER USER******* */

  influencer_user_delete(data){
    return this.http.post('api/v1/admin/pimm/influencer_user_delete', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /**********update_customer_influencer***** */
  
  update_customer_influencer(data){
    return this.http.post('api/v1/admin/pimm/update_customer_influencer', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /***********UPLOAD INFLUENCER IMAGE*********** */

  UploadImage(data){
    return this.http.post('api/v1/admin/pimm/UploadImage', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /***********UPDATE INFLUENCER IMAGE********** */

  UpdateInfluencerImage(data){
    return this.http.post('api/v1/admin/pimm/UpdateInfluencerImage', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /*********UPDATE PAYMENT DETAILS*********** */

  updatePaymentDetails(data){
    return this.http.post('api/v1/admin/pimm/update_payment_Details', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /*********DELETE PAYMENT DETAILS*********** */
  delete_payment_Details(data){
    return this.http.post('api/v1/user/pimm/delete_payment_Details', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  /***********ADD CARD********** */
  create_card(data){
    return this.http.post('/api/v1/user/pimm/update_payment_Details', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }




 /***********ADD CARD********** */
 get_all_questions_by_usertype(){
  return this.http.get('api/v1/question/pimm/admin/get_allquestions_by_usertype').map((data) => {
    return data.json();
  }, error => {
    return error.json();
  });
}

 /***********ADD CARD********** */
 get_all_TransactionsHistory(){
  return this.http.get('api/v1/admin/pimm/transactionHistory').map((data) => {
    return data.json();
  }, error => {
    return error.json();
  });
}


delete_question_by_id(data){
  return this.http.post('api/v1/question/pimm/delete_question_by_id', data).map((data)=>{
    return data.json();
  }, error => {
    return error.json();
  });
}














  add_customer(data){
    return this.http.post('admin/add_customer', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  
  update_customer(data){
    return this.http.post('admin/update_customer', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  deleteCustomer(data){
    return this.http.post('admin/delete_customer', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  add_vendor(data){
    return this.http.post('admin/add_vendor', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  vendor_listing(){
    return this.http.get('admin/vendor_listing').map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  update_vendor(data){
    return this.http.post('admin/update_vendor', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  deleteVendor(data){
    return this.http.post('admin/delete_vendor', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  is_email_exist(data){
    return this.http.post('admin/is_email_exist', data).map((data) => {
      return data.json();
    }, error => {
      return error.json();
    });
  }

  // UploadImage(data){
  //   return this.http.post('upload_image', data).map((data)=>{
  //     return data.json();
  //   }, error => {
  //     return error.json();
  //   });
  // }

  adminLogin(data){
    return this.http.post('admin/admin_login', data).map((data)=>{
      return data.json();
    }, error => {
      return error.json();
    });
  }

  updatePassword(data){
    return this.http.post('admin/admin_password_update', data).map((data)=>{
      return data.json();
    }, error => {
      return error.json();
    });
  }

   UpdateProfile(data){
    return this.http.post('admin/admin_profile_update', data).map((data)=>{
      return data.json();
    }, error => {
      return error.json();
    });
  }


}