import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
	authForm: FormGroup; 
	creditCardForm : FormGroup;
	details: any = JSON.parse(localStorage.getItem('item'));
	Image: any = null;
	selectedFile: any;

	submitted = false;
	cardid : any = '';

	name:any='';
	email:any='';
  	constructor(public apiService: ApiService, public fb: FormBuilder, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, private _location: Location) { 
		  this.createForm();
		  this.createCreditCardForm();
		  this.createForm();
		  this.details = JSON.parse(localStorage.getItem('item'));
		  console.log(this.details.paymentInfo);
		  if(this.details.paymentInfo.length>0){
			  for(var i=0;i<this.details.paymentInfo.length;i++){
				  console.log(this.details.paymentInfo[i]);
				  var num = this.details.paymentInfo[i].number;
				  num = num.toString();
				  this.details.paymentInfo[i].number = num.replace(/.(?=.{4,}$)/g, 'X');
					  console.log(this.details.paymentInfo[i].number);
					  console.log(this.details);
			  }
		  }
  	}

  	ngOnInit() {
  	}

  	createForm(){
	   	this.authForm = this.fb.group({
		    username: [this.details.username, Validators.compose([Validators.required])],
		    email: [this.details.email, Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])]
	    });	  
	 };
	 
	createCreditCardForm(){

		this.creditCardForm = this.fb.group({
		  cardname : ['',[Validators.required]],
		  cardnumber: ['', [Validators.required]],
		  expMonth: ['', [Validators.required]],
		  expYear: ['', [Validators.required]],
		  cvc: ['', [Validators.required]]
		},{});
	
	}

		updateCustomer(){

				this.spinnerService.show();
				let dict = this.authForm.value;
				dict.userid = this.details._id;
				dict.type = 'user';
				this.apiService.update_customer_influencer(dict).subscribe((response) => {
					this.spinnerService.hide();
					if(response.status == 0){
						this._location.back();
						//this.navCtrl.dismiss();
						this.toastr.success(response.msg);
					}else{
						this.toastr.error(response.msg);
					}
				});
		}

  	backView(){
  		this._location.back();
	}

	_deletecarddetails:any='';
	deleteCard(cardid){

		const data = {

			cardid : cardid,
			userid: this.details._id
	
		}

		this._deletecarddetails = data;

	}

	_deleteCard(){

		this.apiService.delete_payment_Details(this._deletecarddetails).subscribe((res)=>{
			console.log(res);
			document.getElementById('closepop').click();
			this.toastr.success("Card Deleted!");
		})

	}

	_addNewCard(){

		// this.apiService.add_card(this._deletecarddetails).subscribe((res)=>{
		// 	console.log(res);
		// 	document.getElementById('closepop').click();
		// 	this.toastr.success("Card Deleted!");
		// })

	}

	// updatePaymentDetails(){
		
	// 	var data = {
	// 	  userid : localStorage.getItem('uid'),
	// 	  card : {
	// 		id : this.randomString(),
	// 		name : value.cardName,
	// 		number: value.number,
	// 		expMonth: value.expMonth,
	// 		expYear: value.expYear,
	// 		cvc: value.cvc,
	// 		cardtype : cardType
	
	// 	  }
	// 	}
	
	// 	console.log(data);
	
	// 	// return false;
	// 	this._userservice.update_payment_Details(data).subscribe((res)=>{
	// 		this.loadingController.dismiss();
	// 		console.log(res);
	// 		this.onReset();
	// 		this._router.navigate(['/profile']);
	// 	})

	// }

	// updateCard(value,cardType){
	// 	var data = {
	// 		userid : localStorage.getItem('uid'),
	// 		card : {
	// 		  id : this.cardid,
	// 		  name : value.cardName,
	// 		  number: value.number,
	// 		  expMonth: value.expMonth,
	// 		  expYear: value.expYear,
	// 		  cvc: null,
	// 		  cardtype : cardType
	  
	// 		}
	// 	  }
	// 	  console.log("SAVE PAYMENT DATA");
	// 	  console.log(data);
	  
	// 	  // return false;
	// 	  this.apiService.edit_card_Details(data).subscribe((res)=>{
			  
	// 		  console.log(res);
	// 		  this.onReset();
	// 		  this.show_profile();
	// 		  setTimeout(() => {
	// 			this.dismiss();
	// 			// this.events.publish('user:profile', this.savedCards);
	// 			// this._router.navigate(['/tabs/profile']);
	// 		  }, 2000);
	  
	// 	  })
	// }
	

	// editCustomerCard(){
	// 	this.submitted = true;
	// 	if (this.creditCardForm.invalid) {
	// 	  return;
	// 	}
  
	// 	this.stripe.setPublishableKey('pk_test_8ghnSkc1YDWnhEuVkhyhHNIc00qj96jauv');
  
	// 	this.stripe.validateCardNumber(value.number)
	// 	  .then((res) => {
	// 		this.present();
	// 		console.log(res);
	// 		if(res == 'OK'){
	// 		  this.validateCard = false;
	// 		  this.stripe.validateExpiryDate(value.expMonth,value.expYear).then((res2)=>{
	// 			  console.log(res2);
	// 			  if(res2=='OK'){
	// 				this.validateExpMonthYear = false;
	// 				this.stripe.validateCVC(value.cvc).then((res3)=>{
	// 				  console.log(res3);
	// 				  if(res3=='OK'){
	// 						this.validateCvv = false;
	// 						this.stripe.getCardType(value.number).then((res4)=>{
  
	// 									console.log(res4);
	// 									/*****UPDATE USER PAYMENT DETAILS****** */
	// 									this.editCard(value,res4);
									
	// 						}).catch((error4) => {
	// 								this.dismiss();
	// 								console.error(error4);
	// 						});
	// 				  }
	// 				}).catch((error3) => {
	// 				  this.dismiss();
	// 				  console.error(error3);
	// 				  // this.validateCvv = true;
	// 				  this.stripe.getCardType(value.number).then((res4)=>{
  
	// 					/*****UPDATE USER PAYMENT DETAILS****** */
	// 					this.editCard(value,res4);
					
	// 				  }).catch((error4) => {
	// 						  this.dismiss();
	// 						  console.error(error4);
	// 				  });
	// 				});
	// 			  }
	// 		  }).catch((error2) => {
	// 			this.dismiss();
	// 			console.error(error2);
	// 			this.validateExpMonthYear = true;
	// 		  });
	// 		}
	// 	  }).catch((error) => {
	// 		this.dismiss();
	// 		console.error(error);
	// 		this.validateCard = true;
	// 	  });
  
  
	// }
}
 