import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	profileData: any = {profile_image: null, fname: '', lname: ''};
  	constructor(public router: Router) {
			if(localStorage.getItem('IsLoggedIn') == 'true'){
  			this.profileData = JSON.parse(localStorage.getItem('profile'))
  		} 
  		console.log('here')
  	}

  	ngOnInit() {
  	}

  	logout(){
		localStorage.removeItem('isLoggedIn');
	  	this.router.navigateByUrl('/');
  	}
}
