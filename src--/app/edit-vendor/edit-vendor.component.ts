import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-vendor',
  templateUrl: './edit-vendor.component.html',
  styleUrls: ['./edit-vendor.component.css']
})
export class EditVendorComponent implements OnInit {
	authForm: FormGroup; 
	details: any = JSON.parse(localStorage.getItem('item'));
	Image: any = null;
	selectedFile: any;
  	constructor(public apiService: ApiService, public fb: FormBuilder, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, private _location: Location) { 
  		this.createForm();
  	}

  	ngOnInit() {
  	}

  	createForm(){
	   	this.authForm = this.fb.group({
		    username: [this.details.username, Validators.compose([Validators.required])],
				email: [this.details.email, Validators.compose([Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])],
				followers : [this.details.followers],
				image : [null]
	    });	   
 	};

 	updateVendor(){
 		if(this.Image == null){
 			this.spinnerService.show();
	 		let dict = this.authForm.value;
			 dict.userid = this.details._id;
			 dict.type = 'influencer';
			 dict.profile_picture = this.details.profile_picture;
			this.apiService.update_customer_influencer(dict).subscribe((response) => {
				this.spinnerService.hide();

				if(response.status == 0){

					this._location.back();
					this.toastr.success('Influencer has been updated successfully!');

				}else{ 

					this.toastr.error('Email already exist in our system.Please try another one!');

				}
			});
 		}else{
 			this.uploadImage();
 		}
 	}

 	onImageChanged(event) {
  		console.log(event)
	    let reader = new FileReader();
	    this.Image = event.target.files[0];
	    this.selectedFile = event.target.files[0];
	    let extn = event.target.value.split('.')[1];
	    if(extn =='jpg' || extn =='png' || extn =='jpeg'){
	      reader.onload = () => {
					this.Image = reader.result;
					console.log(this.Image);
	      };
	      reader.readAsDataURL(event.target.files[0]);               
	    }else if(extn == 'pdf'){
				 this.Image = event.target.files[0].name;
				 console.log(this.Image);
	    }
	    //this.authForm.patchValue({profile_image : this.Image});
		};
		

		public prepareSave(): any {
			let input = new FormData();
			input.append('image', this.authForm.get('image').value);
			return input;
		}



  	uploadImage(){
	    this.spinnerService.show();
	    let dict = {
				'email': this.authForm.value.email,
				'username' : this.authForm.value.username,
	    	'type': 'influencer',
				'userid': this.details._id,
				'profile_picture' : this.details.profile_picture
	    };
	    this.apiService.update_customer_influencer(dict).subscribe((response) => {
	      if(response.status == 0){

					this.authForm.get('image').setValue(this.selectedFile);
					const formModel = this.prepareSave(); 
					console.log(formModel);
	        this.apiService.UploadImage(formModel).subscribe((response2) => {
			
						console.log(response2.data);

						const data = {
							image : 'http://3.16.91.43:3000/images/' + response2.data,
							userid : this.details._id
						}

						this.apiService.UpdateInfluencerImage(data).subscribe((response) => {

								if(response.status == 0){
									this._location.back();
									this.toastr.success("Updated Successfully!");
								}	

						})
	          // this.updateVendor();
	        });
	      }else{
	        this.spinnerService.hide();
	        this.toastr.error(response.msg);
	      }
    	});
  	};

}
 