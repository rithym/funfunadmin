import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/apiService';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {
	authForm: FormGroup; 
	Image: any = null;
	selectedFile: any;

  	constructor(public apiService: ApiService, public fb: FormBuilder, public spinnerService: Ng4LoadingSpinnerService, public toastr: ToastrService, private _location: Location) { 
  		this.createForm();
  	}

  	ngOnInit() {
  	}

	createForm(){
	   	this.authForm = this.fb.group({
		    fname: ['', Validators.compose([Validators.required])],
		    lname: ['', Validators.compose([Validators.required])],
		    email: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])],
	       	contact: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{4,15}')])],
	      	address: ['', Validators.compose([Validators.required])],
	  	 	city: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
	 	 	country: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
	      	profile_image: [null],
	      	password: ['', Validators.compose([Validators.required])],
	      	landmark: ['', Validators.compose([Validators.required])],
	    });	   
 	};

 	addCustomer(){
 		if(this.Image == null){
 			var dict = {
				fname: this.authForm.value.fname,
				lname: this.authForm.value.lname, 
				email: this.authForm.value.email,
				contact: this.authForm.value.contact,
				address: this.authForm.value.address,
				city: this.authForm.value.city,
				country: this.authForm.value.country,
				landmark: this.authForm.value.landmark,
				password: this.authForm.value.password,
				profile_image: this.authForm.value.profile_image,
				payment_mathods: []
			};
			console.log(dict);

			this.spinnerService.show();
			this.apiService.add_customer(dict).subscribe((response) => {
				this.spinnerService.hide();
				if(response.status == 1){
					this._location.back();
					//this.navCtrl.dismiss();
					this.toastr.success('Customer has been added successfully!');
				}else{
					this.toastr.error('Email already exist in our system.Please try another one.');
				}
			});
 		}else{
 			this.uploadImage();
 		}
 	}

  	onImageChanged(event) {
  		console.log(event)
	    let reader = new FileReader();
	    this.Image = event.target.files[0];
	    this.selectedFile = event.target.files[0];
	    let extn = event.target.value.split('.')[1];
	    if(extn =='jpg' || extn =='png' || extn =='jpeg'){
	      reader.onload = () => {
	        this.Image = reader.result;
	      };
	      reader.readAsDataURL(event.target.files[0]);               
	    }else if(extn == 'pdf'){
	       this.Image = event.target.files[0].name;
	    }

	    //this.authForm.patchValue({profile_image : this.Image});
  	};

  	uploadImage(){
	    this.spinnerService.show();
	    let dict = {
	    	'email': this.authForm.value.email,
	    	'item_type': 'customer',
	    	'new': true
	    };
	    this.apiService.is_email_exist(dict).subscribe((response) => {
	      if(response.status == 1){
	        var payload = new FormData();
	        payload.append('image', this.selectedFile, this.selectedFile.name);
	        this.apiService.UploadImage(payload).subscribe((response) => {
	          console.log(response);
	          this.authForm.value.profile_image = response.image;
	          console.log(this.authForm.value);
	          this.Image = null;
	          this.addCustomer();
	        });
	      }else{
	        this.spinnerService.hide();
	        this.toastr.error(response.msg);
	      }
    	});
  	};

}
