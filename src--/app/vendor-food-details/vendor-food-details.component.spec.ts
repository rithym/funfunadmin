import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorFoodDetailsComponent } from './vendor-food-details.component';

describe('VendorFoodDetailsComponent', () => {
  let component: VendorFoodDetailsComponent;
  let fixture: ComponentFixture<VendorFoodDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorFoodDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorFoodDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
